from fastapi.routing import APIRouter
from fastapi.responses import JSONResponse
from pydantic import BaseModel
from jwt_manager import create_token
from schemas.login import user

login_router = APIRouter()

#Autenticacion
@login_router.post('/login', tags=['auth'])
def login(user: user):
    if user.email =="admin@gmail.com" and user.password == "admin":
        token:str = create_token(user.dict())
        return JSONResponse(status_code=200,content=token)
    else:
        return JSONResponse(content="Usuario incorrecto")