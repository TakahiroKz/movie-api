from fastapi.routing import APIRouter
from fastapi import Path, Query, Depends
from fastapi.responses import  JSONResponse
from typing import List
from config.database import session
from fastapi.encoders import jsonable_encoder
from sqlmodel import select
from middlewares.jwt_bearer import JWTBearer
from services.movie import MovieService
from schemas.movie import Movie

movie_routing = APIRouter()

#Retornar todas la peliculas
@movie_routing.get('/movies', tags=['movies'],response_model=List[Movie], status_code=200,dependencies=[Depends(JWTBearer())])
def get_movies() -> List[Movie]:
    db = session
    """statement = select(MovieModel)
    result = db.exec(statement).all()    """
    result = MovieService(db).get_movies()
    return JSONResponse(status_code=200,content=jsonable_encoder(result))

#Retornar pelicula filtrada por ID, Validaciones parametros ruta / Path
@movie_routing.get('/movies/{id}', tags=['movies'],response_model=Movie,status_code=200)
def get_movie(id:int = Path(ge=1, le=2000))-> Movie :
    db = session
    """statement = select(MovieModel).where(MovieModel.id == id)
    result = db.exec(statement).first()"""
    result = MovieService(db).get_movie(id)
    if  not result:
        return JSONResponse(status_code=404,content={'message': 'Pelicula no encontrada'})
    return JSONResponse(status_code=200,content=jsonable_encoder(result))

#Obtener peliculas filtradas por categoria Validacion parametros Query
@movie_routing.get('/movies/',tags=['movies'],response_model=List[Movie],status_code=200)
def get_movies_by_category(category: str = Query(min_length=5, max_length=15)) -> List[Movie]:
    db = session
    """statement = select(MovieModel).where(MovieModel.category == category.capitalize())
    result = db.execute(statement).all()"""
    result = MovieService(db).get_movie_by_category(category.capitalize())
    if not result:
        return JSONResponse(status_code=404, content={'message':'Pelicula no encontrada'})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

#Crear una nueva pelicula
@movie_routing.post('/movies',tags=['movies'],response_model=dict,status_code=201)
def create_movie(movie: Movie) -> dict:
    db = session
    MovieService(db).create_movie(movie)
    return JSONResponse(status_code=201,content={"message":"Se ha registrado la pelicula"})

#Modificar una pelicula
@movie_routing.put('/movies/{id}',tags=['movies'],response_model=dict,status_code=200)
def update_movie(id: int, movie: Movie) -> dict:
    db = session
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={'message':'Pelicula no encontrada'})
    MovieService(db).update_movie(id,movie)
    return JSONResponse(status_code=200, content={"message":"Se ha modificado la pelicula"})

#Eliminar una pelicula
@movie_routing.delete('/movies/{id}',tags=['movies'],response_model=dict,status_code=200) 
def delete_movie(id:int) -> dict:
    db = session
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404,content={"message":"No se encontro la pelicula"})
    MovieService(db).delete_movie(id)
    return JSONResponse(status_code=200, content={"message":"Se ha Eliminado la pelicula"})