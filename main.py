from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from config.database import engine
from sqlmodel import SQLModel
from middlewares.error_handler import ErrorHandler
from routers.movie import movie_routing
from routers.login import login_router

app = FastAPI()
app.title = "Mi aplicacion con FastApi"
app.version = "0.0.1"

app.add_middleware(ErrorHandler)
app.include_router(movie_routing)
app.include_router(login_router)

#Base.metadata.create_all(bind=engine)
SQLModel.metadata.create_all(engine)

#Retornar mensaje simple
@app.get('/', tags=['home']) #tags= permite cambiar un titulo de una API
def message():
    x = "Leonardo Suarez"   
    return HTMLResponse(f'<h1> Hello world {x}</h1>')




