from pydantic import BaseModel, Field
from typing import Optional

class Movie(BaseModel):
    id: Optional[int] = None
    title : str = Field(max_length=50)
    overview : str = Field(min_length=5,max_length=50)
    year : int = Field(le=3000)
    rating : float = Field(ge=1, le=10)
    category : str = Field(min_length=5, max_length=20)

    class Config:
        schema_extra = {
            "example": {
                "id" : 1,
                "title": "Mi pelicula",
                "overview" : "Descripcion de la pelicula",
                "year": 2023,
                "rating" : 10.0,
                "category" : "Acción"
            }
        }
