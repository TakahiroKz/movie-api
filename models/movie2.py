from typing import Optional
from sqlmodel import Field, SQLModel

class Movie(SQLModel, table=True):
    id: Optional[int] = Field(default = None, primary_key=True)
    title : str
    overview : str
    year : int 
    rating : float
    category : str 

